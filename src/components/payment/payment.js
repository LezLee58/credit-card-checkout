import React from 'react';
import './payment.scss';

const payment = () => (
    <form className={"payment"}>
        <div className={"form-row"}>
            <div className={"form-group col-md-8"}>
                <label htmlFor={"card-number"} className={"col-form-label col-form-label-sm text-muted"}>Card Number</label>
                <div class="input-group">
                    <div class="input-group-prepend card-icon">
                        <div class="input-group-text"><i class="fas fa-credit-card"></i></div>
                    </div>
                    <input type={"text"} className={"form-control form-control-lg"} id={"card-number"} defaultValue={"4400 6678 9092 0062"} />
                    <div class="input-group-append visa-icon">
                        <div class="input-group-text"><i class="fab fa-cc-visa"></i></div>
                    </div>
                </div>
            </div>
            <div className={"form-group col"}>
                <label htmlFor={"card-ccv"} className={"col-form-label col-form-label-sm text-muted"}>CCV</label>
                <div class="input-group">
                    <input type={"text"} className={"form-control form-control-lg"} id={"ccv"} defaultValue={"229"} />
                    <div class="input-group-append card-icon">
                        <div class="input-group-text"><i class="fas fa-credit-card"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div className={"form-row"}>
            <div className={"form-group col-md-8"}>
                <label htmlFor={"card-name"} className={"col-form-label col-form-label-sm text-muted"}>Name on card</label>
                <input type={"text"} className={"form-control form-control-lg"} id={"card-name"} defaultValue={"JENNIFER"} />
            </div>
            <div className={"form-group col"}>
                <label htmlFor={"card-mm"} className={"col-form-label col-form-label-sm text-muted"}>MM</label>
                <input type={"text"} className={"form-control form-control-lg"} id={"inputPassword4"} />
            </div>
            <div className={"form-group col"}>
                <label htmlFor={"card-yy"} className={"col-form-label col-form-label-sm text-muted"}>YY</label>
                <input type={"text"} className={"form-control form-control-lg"} id={"inputPassword4"} />
            </div>
        </div>
        <div className={"button-container"}>
            <button type={"submit"} className={"btn btn-payment"}>Place Order</button>
        </div>
    </form >
);

export default payment;