import React, { Component } from 'react';
import './App.scss';

import { Header, Footer } from '../components/layout';
import Checkout from '../components/checkout/checkout';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Checkout />
        <Footer />
      </div>
    );
  }
}

export default App;
