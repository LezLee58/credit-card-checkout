import React from 'react';
import './footer.scss';

export const footer = () => (
    <footer className={'footer bg-dark'}>
        <div className={'container footer-container'}>
            <span>
                Created by <a href={"https://www.linkedin.com/in/leslie-enwerem/"}
                    rel={"noopener noreferrer"}
                    target={"_blank"}
                    title={"Linkedin Profile"}>Leslie Enwerem</a>
            </span>
        </div>
    </footer>
);
