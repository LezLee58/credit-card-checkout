import React from 'react'
import './plan.scss'

class Plan extends React.Component {
    constructor(props) {
        super(props)
        this.data = require('../../static/data/plan.json');
        this.wavesImage = require('../../static/images/react-app-waves.png');
    }

    render() {
        const planDetails = this.data.plan.details.map((detail, index) => {
            return (
                <li key={index}><span className={'fa-li'}><i className={'fas fa-check-circle'}></i></span>{detail}</li>
            )
        })

        return (
            <div className={'plan'}>
                <div className={"plan-info"}>
                    <h3 className={'plan-title'}>Individual Plan</h3>
                    <hr />
                    <p className={'plan-cost'}>{`$${this.data.plan.cost}`}<span>/mo</span></p>
                    <ul className={'plan-details fa-ul'}>
                        {planDetails}
                    </ul>
                    <img src={this.wavesImage} alt={""}></img>
                </div>
            </div>
        )
    }
}

export default Plan