import React from 'react'
import './checkout.scss'
import Plan from '../plan/plan'
import Payment from '../payment/payment'

const checkout = () => (
    <div className={"checkout-container"}>
        <main className={'container'}>
            <div className={'row'}>
                <section className="col-md-12 col-lg-5 plan-col">
                    <div className={'checkout-btn-back-container rounded-circle'}>
                        <i className={"fas fa-arrow-circle-left fa-4x"}></i></div>
                    <Plan />
                </section>
                <section className="col-md-12 col-lg-7 payment-col">
                    <div className={"payment-info-label-container"}>
                        <p>Payment Info</p>
                    </div>
                    <Payment />
                </section>
            </div>
        </main>
    </div>
);

export default checkout;
